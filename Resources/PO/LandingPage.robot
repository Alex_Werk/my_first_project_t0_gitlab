*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${LANDING_NAVIGATION_ELEMENTS_COOKIE} =  xpath=//*[@id="modalWindow"]/div[2]/div[2]
${LANDING_NAVIGATION_COOKIE_BUTTON} =  xpath=//*[@id="modalWindow"]/div[2]/div[2]/wsp-consent-modal/div[2]/div/div[1]/button
${LANDING_NAVIGATION_ELEMENTS} =  xpath=/html/body/div/header/div/div[1]
${LANDING_NAVIGATION_LOGO} =  bol.com


*** Keywords ***
Navigate To
    go to  ${URL}

Verify Cookie Page
    Wait Until Page Contains Element  ${LANDING_NAVIGATION_ELEMENTS_COOKIE}
    Sleep  3s
    Click Button  ${LANDING_NAVIGATION_COOKIE_BUTTON}
    Sleep  3s

Verify Page Loaded
    Page Should Contain  ${LANDING_NAVIGATION_LOGO}
    Sleep  3s